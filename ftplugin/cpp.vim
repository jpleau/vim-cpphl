augroup cpphi
	autocmd!
	autocmd BufWinEnter,BufWritePost *.{cpp,hpp,h,cc,cxx} :call cpphl#AutoHL()
augroup END

