if !exists("g:cpp_custom_highlight_autorun")
	let g:cpp_custom_highlight_autorun = 0
endif

if !exists("g:cpp_custom_highlight_includes")
	let g:cpp_custom_highlight_includes = ""
endif

if !exists("g:cpp_custom_light_clang_exec")
	let g:cpp_custom_light_clang_exec = "/usr/bin/clang++"
endif

function! cpphl#ToggleAutoHL()
	if g:cpp_custom_highlight_autorun
		let g:cpp_custom_highlight_autorun = 0
	else
		let g:cpp_custom_highlight_autorun = 1
		call cpphl#HL()
	endif
endfunction

function! cpphl#AutoHL()
	if g:cpp_custom_highlight_autorun
		call cpphl#HL()
	endif
endfunction

function! cpphl#Strip(input_string)
    return substitute(a:input_string, '^\s*\(.\{-}\)\s*$', '\1', '')
endfunction

function! cpphl#ProcessLine(line)
	let l:keyword = cpphl#Strip(a:line)
	if l:keyword != ""
		if !has_key(b:cpp_custom_highlight_keywords, l:keyword)
			let b:cpp_custom_highlight_keywords[l:keyword] = 1
			let l:syntax = "syn match cppCustomType \"\\v(%s::|\\~|\\w)@<!%s(\\w|\\(|\\s\\()@!\""
			let l:syntax = printf(l:syntax, l:keyword, l:keyword)
			exec l:syntax
		endif
	endif
endfunction

function! cpphl#HiType()
	if !exists("b:cpp_custom_light_link")
		hi link cppCustomType StorageClass
		let b:cpp_custom_light_link = 1
	endif
endfunction

function! cpphl#JobHandler(job_id, data, event)
	if a:event == "stdout"
		for l:line in a:data
			call cpphl#ProcessLine(l:line)
		endfor
	else
		call cpphl#HiType()
	endif
endfunction

function! cpphl#HL()
	if exists("g:cpp_custom_highlight_run")

		if !exists("b:cpp_custom_highlight_keywords")
			let b:cpp_custom_highlight_keywords = {}
		endif

		let l:filepath = expand("%:p")
		let l:dir = expand("%:p:h")

		let g:cpp_flags = [
			\ "-x", "c++", "-std=c++14",
			\ "-I", ".",
			\ "-isystem", "/usr/include",
			\ "-isystem", "/usr/local/include",
			\ "-isystem", "/usr/include/c++",
			\ "-isystem", "/usr/lib/llvm-3.6/include/",
		\ ]

		let l:command = [g:cpp_custom_light_clang_exec] + g:cpp_flags

		let l:args = [
			\ g:cpp_custom_highlight_run,
			\ l:dir,
			\ l:filepath,
		\ ]

		let l:args = l:args + l:command

		if has("nvim")
			let s:callbacks = {
			\ 'on_stdout': function('cpphl#JobHandler'),
			\ 'on_exit': function('cpphl#JobHandler'),
			\ }

			let s:job = jobstart(l:args, s:callbacks)
		else
			let l:fullcmd = join(l:args, ' ')
			let l:res = split(system(l:fullcmd . " 2&> /dev/null"), '\n')
			for l:line in l:res
				call cpphl#ProcessLine(l:line)
				call cpphl#HiType()
			endfor
		endif
	endif
endfunction

