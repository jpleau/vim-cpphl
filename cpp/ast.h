#include <memory>

#include <clang/AST/ASTContext.h>
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include <clang/Frontend/FrontendAction.h>

#include <iostream>

class MyVisitor : public clang::RecursiveASTVisitor<MyVisitor> {
public:
	using TypeList = std::vector<std::string>;
	explicit MyVisitor(clang::CompilerInstance *compiler) : 
		context(&compiler->getASTContext()) {
	}

	bool VisitDecl(clang::Decl *);
	auto& get_types() const { return types; }
	void print_types() const;

	clang::SourceManager *manager = nullptr;
private:
	void processType(const std::string &);
	clang::ASTContext *context;
	TypeList types;
};

class MyConsumer : public clang::ASTConsumer {
public:
	explicit MyConsumer(clang::CompilerInstance *compiler)
		: visitor(new MyVisitor(compiler)) {
	}
	virtual void HandleTranslationUnit(clang::ASTContext &Context) override;
private:
	MyVisitor *visitor;
};

class MyAction : public clang::ASTFrontendAction {
public:
	virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance &, llvm::StringRef) override;
};

