#include <clang/Basic/LLVM.h>

#include <algorithm>
#include <vector>
#include <string>
#include <regex>

#include "ast.h"

std::regex extract_name(R"(^(?!_)(class |struct )*(.*?)(<.*)?$)");

std::unique_ptr<clang::ASTConsumer> MyAction::CreateASTConsumer(clang::CompilerInstance &compiler, llvm::StringRef InFile) {
	return std::unique_ptr<clang::ASTConsumer>(new MyConsumer(&compiler));
}

void MyConsumer::HandleTranslationUnit(clang::ASTContext &Context) {
	visitor->manager = &Context.getSourceManager();
	visitor->TraverseDecl(Context.getTranslationUnitDecl());
	visitor->print_types();
}

void MyVisitor::print_types() const {
	for (auto &t : types) {
		std::cout << t << std::endl;
	}
}


void MyVisitor::processType(const std::string &type_str){
	std::string result;
	std::smatch smatch;
	bool match = std::regex_match(type_str, smatch, extract_name);
	if (match) {
		result = smatch[2];
	}

	if (std::find(types.begin(), types.end(), result) == types.end()) {
		types.push_back(result);
	}
}

bool MyVisitor::VisitDecl(clang::Decl *decl){
	// typedefs and aliases
	if (clang::TypeAliasDecl::classof(decl) || clang::TypedefDecl::classof(decl)) {
		clang::NamedDecl  *n = static_cast<clang::TypeAliasDecl *>(decl);
		processType(n->getName());
	}

	// type declarations
	if (clang::TypeDecl::classof(decl)) {
		clang::NamedDecl *n = static_cast<clang::TypeDecl *>(decl);
		processType(n->getName());
	}

	return true;
}
