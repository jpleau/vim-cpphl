#include "ast.h"
#include <algorithm>
#include <vector>
#include <clang/Frontend/FrontendActions.h>
#include <clang/Tooling/Tooling.h>

using namespace clang::tooling;

class MyCompilationDB : public CompilationDatabase {
public:
	explicit MyCompilationDB(std::vector<std::string>& cmds, std::string& path, std::vector<std::string>& sources) {
		commands.emplace_back(path, cmds);
		files.resize(sources.size());
		files.insert(files.begin(), sources.begin(), sources.end());
	}
	std::vector<CompileCommand> getCompileCommands(clang::StringRef FilePath) const override {
		std::vector<CompileCommand> ret(commands);
		ret[0].CommandLine.push_back(FilePath);
		return ret;
	}

	std::vector<std::string> getAllFiles() const override {
		return files;
	}

	std::vector<CompileCommand> getAllCompileCommands() const override {
		std::vector<CompileCommand> ret;
		std::for_each(files.begin(), files.end(), [&ret, this](auto &file) {
			auto new_commands = this->getCompileCommands(file);
			ret.resize(ret.size() + new_commands.size());
			ret.insert(ret.end(), new_commands.begin(), new_commands.end());
		});
		return ret;
	}
private:
	std::vector<CompileCommand> commands;
	std::vector<std::string> files;
};

int main(int argc, const char **argv) {
	if (argc > 2) {
		std::string path = argv[1];
		std::vector<std::string> sources { argv[2] };

		std::vector<std::string> command;
		if (argc > 3) {
			for (int i = 3; i < argc; ++i) {
				command.push_back(argv[i]);
			}
		} else {
			command = {
				"clang++-3.6", "-x", "c++", "-std=c++14",
				"-I", path,
				"-isystem", "/usr/include",
				"-isystem", "/usr/local/include",
				"-isystem", "/usr/include/c++",
				"-isystem", "/usr/lib/llvm-3.6/lib/clang/3.6.2/include",
				"-isystem", "/usr/lib/llvm-3.6/include/",
			};
		}
		MyCompilationDB db(command, path, sources);

		ClangTool tool(db, sources);
		tool.run(newFrontendActionFactory<MyAction>().get());
		return 0;
	}
}
